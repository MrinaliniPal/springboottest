package com.techl33t.starter.test.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.techl33t.starter.test.pojo.Articles;

@Transactional
@Repository
public class KspArticleDAO {
	@PersistenceContext	
	private EntityManager entityManager;
	
	public Articles getArticleById(int id) {
		return entityManager.find(Articles.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Articles> getAllArticles() {
		String hql = "FROM Articles as a ORDER BY a.id";
		return (List<Articles>) entityManager.createQuery(hql).setMaxResults(10).getResultList();
	}
}
