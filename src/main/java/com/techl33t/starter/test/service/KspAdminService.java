package com.techl33t.starter.test.service;

import java.util.List;

import com.techl33t.starter.test.pojo.Articles;

public interface KspAdminService {
	public List<Articles> getAllArticles();
	public Articles getArticleById(int id);
}
