package com.techl33t.starter.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techl33t.starter.test.dao.KspArticleDAO;
import com.techl33t.starter.test.pojo.Articles;

@Service
public class KspAdminServiceImpl implements KspAdminService{

	@Autowired
	private KspArticleDAO articleDao;
	
	@Override
	public List<Articles> getAllArticles() {
		// TODO Auto-generated method stub
		return articleDao.getAllArticles();
	}

	@Override
	public Articles getArticleById(int id) {
		// TODO Auto-generated method stub
		Articles obj = articleDao.getArticleById(id);
		return obj;
	}

}
