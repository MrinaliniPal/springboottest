package com.techl33t.starter.test.testController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techl33t.starter.test.pojo.Articles;
import com.techl33t.starter.test.service.KspAdminService;

@RestController
@RequestMapping("ksp_api")
public class TestController {
	@Autowired
	private KspAdminService articleService;
	
	@GetMapping("/articles")
	public ResponseEntity<List<Articles>> getAllArticles() {
		List<Articles> list = articleService.getAllArticles();
		return new ResponseEntity<List<Articles>>(list, HttpStatus.OK);
	}
	
	@PostMapping("/article")
	public ResponseEntity<Articles> getArticleById(@RequestBody Articles article){
		Articles obj = articleService.getArticleById(article.getId());
		return new ResponseEntity<Articles>(obj, HttpStatus.OK);
		
	}

}
